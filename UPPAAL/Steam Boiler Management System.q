//This file was generated from (Commercial) UPPAAL 4.0.14 (rev. 5615), May 2014

/*
checks for deadlock in the system
*/
A[] not deadlock

/*
If water Level is out of bounds, the boiler and both pumps are off
*/
A[]((w < WaterLevelObserver0.w1 || w > WaterLevelObserver0.w2) imply ( BoilerController0.BOILER_OFF && pump1On == 0 && pump2On == 0)) \


/*
In Some state the boiler comes on
*/
E<> BoilerController0.BOILER_ON\


/*
on some path the specified combinations of on statuses match the rules in the the truth tables - Checking the integrity of the logic behind PumpOnStatus truth table
note: state 1 omitted as both pumps cannot be on at the same time
*/
A<> ( \
	((pump1On == 0 && pump2On ==0) imply (pumpOnStatus == 0)) &&\
	((pump1On == 1 && pump2On ==0) imply (pumpOnStatus == 2)) &&\
	((pump1On == 0 && pump2On ==1) imply (pumpOnStatus == 3))\
)	

/*
on some path the specified combinations of online statuses match the rules in the the truth tables - Checking the integrity of the logic behind PumpOnStatus truth table
*/
E<> ( \
	((pump1Online == 0 && pump2Online ==0) imply (pumpOnlineStatus == 0)) &&\
	((pump1Online == 1 && pump2Online ==1) imply (pumpOnlineStatus == 1)) &&\
	((pump1Online == 1 && pump2Online ==0) imply (pumpOnlineStatus == 2)) &&\
	((pump1Online == 0 && pump2Online ==1) imply (pumpOnlineStatus == 3))\
)	

/*
Checking the integrity of the logic behind PumpOnStatus truth table
note: state 1 omitted as both pumps cannot be on at the same time
*/
E<> ( (PumpOnStatusObserver0.DECISION) imply (pumpOnStatus == 0 || pumpOnStatus == 2 || pumpOnStatus == 3 ))

/*
The pumps must be only ever on 1 at a time OR both off
\/\/rules check
*/
A[] (   (pump1On== 1 && pump2On == 0) || (pump1On== 0 && pump2On == 1) || (pump1On== 0 && pump2On == 0)   ) 

/*
If Steam level is out of bounds, the boiler is off
*/
A[]((s > SteamObserver0.maxSteam) imply ( BoilerController0.BOILER_OFF)) \


/*
Both Pumps are Never On at the same time
*/
A[] (!(pump1On == 1 && pump2On == 1))

/*
Steam Level Bounds Check
*/
A[] (s < SteamObserver0.maxSteam) 

/*
Water Level Bounds Check
*/
A[] (w > WaterLevelObserver0.w1 || w < WaterLevelObserver0.w2) \

